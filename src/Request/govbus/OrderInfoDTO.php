<?php
/**
 * User:  MaxZhang
 * Email: q37388438@gmail.com
 * Date: 2019/06/10
 * Time: 19:37
 */

namespace MaxZhang\SuningSdk\Request\Govbus;


class OrderInfoDTO {

    private $apiParams = array();

    private $gcOrderNo;

    public function getGcOrderNo() {
        return $this->gcOrderNo;
    }

    public function setGcOrderNo($gcOrderNo) {
        $this->gcOrderNo = $gcOrderNo;
        $this->apiParams["gcOrderNo"] = $gcOrderNo;
    }

    public function getApiParams(){
        return $this->apiParams;
    }

}