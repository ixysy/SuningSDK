<?php
/**
 * User:  MaxZhang
 * Email: q37388438@gmail.com
 * Date: 2019/06/10
 * Time: 19:37
 */

namespace MaxZhang\SuningSdk\Request\Govbus;



class OrderItemIds {

    private $apiParams = array();

    private $orderItemId;

    private $skuId;

    public function getOrderItemId() {
        return $this->orderItemId;
    }

    public function setOrderItemId($orderItemId) {
        $this->orderItemId = $orderItemId;
        $this->apiParams["orderItemId"] = $orderItemId;
    }

    public function getSkuId() {
        return $this->skuId;
    }

    public function setSkuId($skuId) {
        $this->skuId = $skuId;
        $this->apiParams["skuId"] = $skuId;
    }

    public function getApiParams(){
        return $this->apiParams;
    }

}